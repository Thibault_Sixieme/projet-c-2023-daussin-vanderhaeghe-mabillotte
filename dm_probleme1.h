#ifndef _DM_PROBLEME1_H_
#define _DM_PROBLEME1_H_

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct grille_s grille;

grille initialiser_grille(int);
void afficher_grille(grille);
void detruire_grille(grille* );
int case_valide(grille, int, int);
int nombres_de_solutions(grille, int, int);
int nombre_croix(grille);
void vfinal(int);

#endif
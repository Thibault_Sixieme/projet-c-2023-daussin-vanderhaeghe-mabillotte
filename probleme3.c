#include "probleme3.h"



typedef int** grille ;

struct miroir_s {
    int x;
    int y;
    bool dia;
    bool* passage;
    bool est_passe;
};

typedef struct miroir_s miroir;

typedef struct miroir_s * contenu;

struct maillon_s {
    contenu valeur;
    struct maillon_s* suivant;
};

typedef struct maillon_s maillon;

typedef maillon* pile;


pile creer_pile(void) {
    return NULL;
}

bool est_vide_pile(pile p) {
    return p == NULL;
}

int taille_pile(pile p) {
    if (p==NULL) {
        return 0;
    }
    else {
        return 1 + taille_pile(p->suivant);
    }
}

contenu sommet_pile(pile p) {
    assert(p!=NULL);
    return p->valeur;
}

void empiler(contenu elt,pile* p) {
    maillon* m = malloc(sizeof(maillon));
    m->valeur = elt;
    m->suivant = *p;
    *p = m;
}

contenu depiler(pile* p) {
    assert(!est_vide_pile(*p));
    contenu elt = (*p)->valeur;
    maillon* m = *p;
    *p = m->suivant;
    free(m);
    return elt;
} 

void detruire_pile(pile p) {
    if (est_vide_pile(p)) {
    }
    else {
    detruire_pile(p->suivant);
    free(p);
    }
}


grille creer_grille_et_miroir(int* nb_l, int* nb_c,int* nb_m, miroir** miroirs, char* fichier) {
    FILE* doc = fopen(fichier,"r");
    fscanf(doc,"%d\n",nb_l);
    fscanf(doc,"%d\n",nb_c);
    fscanf(doc,"%d\n",nb_m);
    grille g = malloc(sizeof(int*)*(*nb_l));
    for (int i=0;i< *nb_l;i+=1) {
        int* g_l = malloc(sizeof(int)*(*nb_c));
        for (int j=0;j< *nb_c;j+=1) {
            g_l[j] = -1;
        }
        g[i] = g_l;
    }
    *miroirs = malloc(sizeof(miroir)* (*nb_m));
    for (int i=0;i< *nb_m;i+=1) {
        int x;
        int y;
        fscanf(doc,"%d %d\n",&x,&y);
        (*miroirs)[i].x = x;
        (*miroirs)[i].y = y;
        (*miroirs)[i].dia = true;
        (*miroirs)[i].passage = malloc(sizeof(bool)*4);
        (*miroirs)[i].passage[0] = false;
        (*miroirs)[i].passage[1] = false;
        (*miroirs)[i].passage[2] = false;
        (*miroirs)[i].passage[3] = false;
        (*miroirs)[i].est_passe = false;
        g[y][x] = i;
    }
    fclose(doc);
    return g;
}

void detruire_grille_et_miroir(grille g,int l,miroir* miroirs,int nb_m) {
    for (int i=0;i<l;i+=1) {
        free(g[i]);
    }
    for (int i=0;i<nb_m;i+=1) {
        free(miroirs[i].passage);
    }
    free(miroirs);
    free(g);
    
} 

int changer_direction(bool dia,int dir) {
    // Cette fonction prend en paramètre une direction sous la forme 0 1 2 ou 3. 0 = on se dirige vers le l'est. 1 = on se dirige vers le nord ...
    // Elle renvoie une direction selon la disposition du miroir.
    if (dir==0) {
        if (dia) {
            return 3;
        }
        else {
            return 1;
        }
    }
    if (dir==1) {
        if (dia) {
            return 2;
        }
        else {
            return 0;
        }
    }
    if (dir==2) {
        if (dia) {
            return 1;
        }
        else {
            return 3;
        }
    }
    if (dir==3) {
        if (dia) {
            return 0;
        }
        else {
            return 2;
        }
    }
    else {
        assert(false);
    }
}

bool grille_valide(grille g,int nb_l, int nb_c,miroir* miroirs) {
    // Cette fonction prend en paramètre une grille et un tableau de miroir.
    // Elle renvoie un booléen indiquant si la dispotion des miroirs du tableau permet
    // de sortie de la grille.
    //Inv(miroirs) : true <=> il existe m_1,...,m_n miroirs dans le tableau miroirs (bon effectivement le pluriel est embétant ici) 
    //tel qu'il existe un chemin m_1-...-m_n qui mène à la sortie. 
    int x = -1;
    int y = 0;
    int dir = 0;
    int anti_boucle = 0;
    while (anti_boucle < 100000) {
        anti_boucle += 1;
        if (dir==0) {
            x+=1;
        }
        if (dir==1) {
            y-=1;
        }
        if (dir==2) {
            x-=1;
        }
        if (dir==3) {
            y+=1;
        }
        if (x== nb_c && y==0) {
                return true;
            }
        if (x < 0 || y < 0 || x >= nb_c || y >= nb_l) {
            return false;
        }
        else {
            if (g[y][x]!=-1) {
                dir = changer_direction(miroirs[g[y][x]].dia,dir);
            }
        }
    }
    return false;
}

//fonction explicite
void afficher_miroirs(miroir* miroirs,int nb_m) {
    for (int i=0;i<nb_m;i+=1) {
        if (miroirs[i].dia) {
            printf("\\");
        }
        else {
            printf("/");
        }
    }
    printf("\n");
}

//fonction explicite
void afficher_grille(grille g, int nb_l, int nb_c, miroir* miroirs, int nb_m) {
    for (int i=0;i<nb_l;i+=1) {
        for (int j=0;j<nb_c;j+=1) {
            if (g[i][j]==-1) {
                printf(" ");
            }
            else {
                if (miroirs[g[i][j]].dia) {
                    printf("\\");
                }
                else {
                    printf("/");
                }
            }
        }
        printf("\n");
    }
}


miroir* copie_miroirs(miroir* miroirs,int nb_m) {
    // Cette fonction prend un tableau de miroir en paramèter
    // Elle renvoie une copie du tableau passé en paramète (les objets sont des copies mais des instances différente qui ne
    //posoèdent pas la même adresse).
    miroir* miroirs_bis = malloc(sizeof(miroir)*nb_m);
    for (int i=0;i<nb_m;i+=1) {
        bool* passage_bis = malloc(sizeof(bool)*4);
        miroirs_bis[i].x = miroirs[i].x;
        miroirs_bis[i].y = miroirs[i].y;
        miroirs_bis[i].dia = miroirs[i].dia;
        passage_bis[0] = miroirs[i].passage[0];
        passage_bis[1] = miroirs[i].passage[1];
        passage_bis[2] = miroirs[i].passage[2];
        passage_bis[3] = miroirs[i].passage[3];
        miroirs_bis[i].passage = passage_bis;
        miroirs_bis[i].est_passe = miroirs[i].est_passe;

    }
    return miroirs_bis;
}

//fonction explicite
void detruire_miroirs(miroir* miroirs,int nb_m) {
    for (int i=0;i<nb_m;i+=1) {
        free(miroirs[i].passage);
    }
    free(miroirs);
}


bool compare_miroirs(miroir* miroirs1,miroir* miroirs2,int nb_m) {
    // Cette fonction prend en paramètre deux de tableaux de miroirs
    // Elle renvoie un booléen indiquant si les miroirs construisent le même chemin 
    // c'est à dire si un rayon qui parcours l'ensemble de miroirs prend les
    // mêmes direction. 
    for (int i=0;i<nb_m;i+=1) {
        for (int j=0;j<4;j+=1) {
            if (miroirs1[i].passage[j] != miroirs2[i].passage[j]) {
                return false;
            }
        }
    }
    return true;
}

void ajoute_si_pas_present (miroir* miroirs,int nb_m,pile* p) {
    // Cette fonction prend en paramètre un tableau de miroirs et une pile
    // Elle empile le tableau de miroirs dans la pile si le chemin construit par le tableau
    // n'est pas déjà present dans la pile. Sinon elle détruit le tableau.
    pile p2 = creer_pile();
    bool present = false;
    while (!est_vide_pile(*p) && !present) {
        miroir* miroirs2 = depiler(p);
        empiler(miroirs2,&p2);
        if (compare_miroirs(miroirs,miroirs2,nb_m)) {
            present = true;
            detruire_miroirs(miroirs,nb_m);
        }
    }
    while (!est_vide_pile(p2)) {
        miroir* miroirs3 = depiler(&p2);
        empiler(miroirs3,p);
    }
    if (!present) {
        empiler(miroirs,p);
    }
}

void evaluer_grille(grille g,int nb_l,int nb_c,miroir* miroirs,int nb_m,pile* p) {
    // Cette fonction prend en paramètre une grille et un tableau de miroir ainsi qu'une pile.
    // Elle empile le tableau de miroirs dans la pile si si la dispotion des miroirs du tableau permet
    // de sortie de la grille et si le chemin construit par les miroirs n'est pas déjà dans la pile.
    //L'invariant est similaire à la fonction grille_valide.
    int x = -1;
    int y = 0;
    int dir = 0;
    int anti_boucle = 0;
    bool fin = false;
    miroir* miroirs_bis = copie_miroirs(miroirs,nb_m);
    while (anti_boucle < 100000 && !fin) {
        anti_boucle += 1;
        if (dir==0) {
            x+=1;
        }
        if (dir==1) {
            y-=1;
        }
        if (dir==2) {
            x-=1;
        }
        if (dir==3) {
            y+=1;
        }
        if (x== nb_c && y==0) {
                ajoute_si_pas_present(miroirs_bis,nb_m,p);
                fin = true;
            }
        else { 
            if (x < 0 || y < 0 || x >= nb_c || y >= nb_l) {
                assert(1==3); //impossible en pratique
            }
            else {
                if (g[y][x]!=-1) {
                    miroirs_bis[g[y][x]].passage[changer_direction(miroirs[g[y][x]].dia,dir)] = true;
                    dir = changer_direction(miroirs[g[y][x]].dia,dir);
                }
            }
        }
    }
}

int v1(grille g,int nb_l,int nb_c,miroir* miroirs,int nb_m) {
    int com = 0;
    pile p = creer_pile();
    void v1_aux(int id_m,bool dia) {
        //Fonction récursive donc on fait un résonnement par récurrence fini sur [0;n-1] avec n le nombre de miroirs :
        // H_n " v1_aux(n,dia) empile toutes les solutions avec les miroirs m_0, ... , m_(n-1) fixés et m_n = dia"
        // A la fin v1_aux(0,dia) + v1_aux(0,not dia) à trouver toutes solutions possibles (avec doublons)  
        if (id_m == nb_m-1) {
            miroirs[id_m].dia = dia;
            com+=1;
            if (grille_valide(g,nb_l,nb_c,miroirs)) {
                // on est obligé d'évaluer la grille pour vérifié que le chemin n'est pas déjà dans la pile 
                evaluer_grille(g,nb_l,nb_c,miroirs,nb_m,&p);
            }
            else {
            }
        }
        else {
            miroirs[id_m].dia = dia;
            // on fait deux appels récursif, pour un des appels le miroir en position id_m+1 est en diagonal
            // l'autre en anti-diagonal. 
            v1_aux(id_m+1,dia);
            v1_aux(id_m+1,!dia);
        }
    }
    v1_aux(0,true);
    v1_aux(0,false);
    int nb_sol = 0;
    while (!est_vide_pile(p)) {
        miroir* miroirs_ = depiler(&p);
        nb_sol += 1;
        detruire_miroirs(miroirs_,nb_m);
    }
    return nb_sol;
}





int v2(grille g,int nb_l,int nb_c,miroir* miroirs,int nb_m) {
    int v2_aux(int x, int y, int dir, miroir* miroirs,int nb_m) {
        int anti_boucle = 0;
        while (anti_boucle < 100) {
            if (dir==0) {
                x+=1;
            }
            if (dir==1) {
                y-=1;
            }
            if (dir==2) {
                x-=1;
            }
            if (dir==3) {
                y+=1;
            }
            if (x== nb_c && y==0) {
                return 1;
            }
            if (x < 0 || y < 0 || x >= nb_c || y >= nb_l) {
                return 0;
            }
            if (g[y][x]!=-1) {
                if (!miroirs[g[y][x]].est_passe) {
                    int d0 = 0;
                    int d1 = 0;
                    miroir* miroirs_bis = copie_miroirs(miroirs,nb_m); 
                    miroirs_bis[g[y][x]].dia = true;
                    miroirs_bis[g[y][x]].est_passe = true;
                    d0 = v2_aux(x,y,changer_direction(true,dir),miroirs_bis,nb_m);
                    detruire_miroirs(miroirs_bis,nb_m);
                    miroir* miroirs_bis_2 = copie_miroirs(miroirs,nb_m); 
                    miroirs_bis_2[g[y][x]].dia = false;
                    miroirs_bis_2[g[y][x]].est_passe = true;
                    d1 = v2_aux(x,y,changer_direction(false,dir),miroirs_bis_2,nb_m);
                    detruire_miroirs(miroirs_bis_2,nb_m);
                    return d0 + d1;
                }
                else {
                    dir = changer_direction(miroirs[g[y][x]].dia,dir);
                }
                
            }
            anti_boucle += 1;
        }
    }
    v2_aux(-1,0,0,miroirs,nb_m);
}

int vfinale(grille g,int nb_l,int nb_c,miroir* miroirs,int nb_m) {
    return v2(g,nb_l,nb_c,miroirs,nb_m);
}


int main(int argc, char* argv[]) {
    if (argc != 2) {
        printf("Les arguments ne sont pas bons, cette commande prend en paramètre le nom d'un fichier");
        assert(false);
    }
    else {
        int l;
        int c;
        int m;
        miroir* miroirs;
        grille g = creer_grille_et_miroir(&l,&c,&m,&miroirs,argv[1]);
        printf("%d\n",vfinale(g,l,c,miroirs,m));
        detruire_grille_et_miroir(g,l,miroirs,m);
    }
}

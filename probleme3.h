#ifndef _PROBLEME3_H_
#define _PROBLEME3_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <assert.h>

typedef int** grille ;

typedef struct miroir_s miroir;


int v1(grille,int,int,miroir*,int);
/*
0. Membre(s) du groupe ayant participé à cette version :
	* Kellian 
	* Mathieu
    * Thibault
1. Brève description de la méthode utilisée :
	Cette fonction implémente une version naïve de la résolution du problème avec :
	* On utilise une méthode brute force : on se contente de tester toutes les combinaisons possibles.
2. Plus d'explications si nécessaire :
	* Le problème de cette méthode est qu'elle donne des solutions doublons (cf mail). Il a donc fallu batailler un petit peu pour écarter les
	les chemins en trop. Pour ce faire, quand on trouve une solution qui marche, on évalue le chemin réel en prenant en comptant les directions que prend le rayon.
	Si le chemin n'a encore été vu, on l'empile dans une pile, sinon on ne fait rien. A la fin renvoie la taille de la pile.
3. Discussion sur l'efficacité de la fonction :
	La complexité de cette fonction est O(l^2 * 2^n) : en effet, si on considère qu'une solution potentielle est une liste de n miroirs 
    en diagonal "\" ou en anti-diagonal "/", alors on peut assimiler une solution potentielle à une n-liste d'élément de {0;1} :
    il y a donc 2^n possibilités à testées. 
	De plus tester si une solution est valide a un cout quadratic en termes de longueur grille qu'on note l.
	En effet pour tester une solution on parcours la grille de la même façon que le ferrait le rayon : dans
	le pire des cas on parcourt toute la grille le coût est donc quadratic. 
	Créer et tester toutes les solutions potentielles à un coup de l*2 * n^2.
*/

int v2(grille,int,int,miroir*,int);
/*
0. Membre(s) du groupe ayant participé à cette version :
	* Kellian
	* Mathieu
	* Thibault
1. Brève description de la méthode utilisée :
	Cette fonction implémente une version amélioré de la résolution du problème avec :
	* On utilise un méthode de back tracking avec élagage de l'arbre. 
2. Plus d'explications si nécessaire :
	* On construit les solutions potentiels au fur et à mesure en parcourant la grille à la façon dont le ferait le rayon.
	Si on rencontre un miroir m pour la première fois : on a donc deux façon de continuer on lance donc deux appels récursif avec
	des directions différente.
	Si il n'y a pas de miroir on continue dans la même direction.
	Si il y a un miroir m déjà rencontré, sa position est fixé on se contente de suivre la déviation du miroir.
	Si on arrive sur un bord de la grille le chemin n'est pas correcte on renvoie 0.
	Si on arrive à la sortie le chemin est correcte on renvoie 1.
3. Discussion sur l'efficacité de la fonction :
	Il est très dur de determiné la complexité d'un algorithme qui utilise la méthode du backtracking (avec des élagages) mais toujours est-il
	que cette deuxième version est beaucoup plus efficace que la première. En effet les élagages permettent d'éliminer des cas
	inutiles à testés. Par exemple si on prend l'exemple donné avec le sujet. Le premier miroir envoie soit vers le haut soit vers le 
	bas. La première version aurait tester toutes les combinaisons possible avec le premier miroir orienté vers le haut ce qui est initile.
	Cette version élimine directement toute ces combinaisons : il est donc clair qu'elle est plus efficace. 
*/



#endif 
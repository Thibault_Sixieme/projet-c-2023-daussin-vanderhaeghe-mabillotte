#include "dm_probleme1.h"

// Définition du type case
typedef enum {VIDE, CROIX} case_s ;

// Définition du type grille
struct grille_s {
    int taille ;
    case_s** cases ;
};

typedef struct grille_s grille;

// Fonction pour initialiser une grille vide de taille n
grille initialiser_grille(int n) {
    grille g;
    g.taille = n;
    g.cases = malloc(n * sizeof(case_s*));
    for (int i = 0; i < n; i+=1) {
        g.cases[i] = malloc(n * sizeof(case_s));
        for (int j = 0; j < n; j++) {
            g.cases[i][j] = VIDE;
        }
    }
    return g;
}

// Fonction pour afficher une grille
void afficher_grille(grille g) {
    for (int i = 0; i < g.taille; i+=1) {
        for (int j = 0; j < g.taille; j+=1) {
            if (g.cases[i][j] == VIDE) {
                printf("_");
            } else if (g.cases[i][j] == CROIX) {
                printf("X");
            }
            if (j != g.taille - 1) {
                printf("|");
            }
        }
        printf("\n");
        if (i != g.taille - 1) {
            for (int k = 0; k < g.taille * 2 - 1; k+=1) {
                printf("-");
            }
            printf("\n");
        }
    }
}

// Fonction pour libérer la mémoire allouée par une grille
void detruire_grille(grille* g) {
    // Libérer la mémoire allouée pour chaque case individuelle
    for (int i = 0; i < g->taille; i+=1) {
        free(g->cases[i]);
    }
    // Libérer la mémoire allouée pour le tableau de pointeurs de cases
    free(g->cases);
    // Libérer la mémoire allouée pour la structure grille
}

int case_valide(grille g, int ligne, int colonne)
{
    // Vérifier la ligne
    for (int j = 0; j < g.taille; j+=1) {
        if (g.cases[ligne][j] == CROIX && j != colonne) {
            return 0;
        }
    }
    
    // Vérifier la colonne
    for (int i = 0; i < g.taille; i+=1) {
        if (g.cases[i][colonne] == CROIX && i != ligne) {
            return 0;
        }
    }
    
    // Vérifier les diagonales
    for (int i = 0; i < g.taille; i+=1) {
        for (int j = 0; j < g.taille; j+=1) {
            if (i + j == ligne + colonne || i - j == ligne - colonne) {
                if (g.cases[i][j] == CROIX && (i != ligne || j != colonne)) {
                    return 0;
                }
            }
        }
    }
    
    // Si toutes les contraintes sont respectées, la case est valide
    return 1;
}

int nombre_croix(grille g) {
    int n = 0;
    for(int i=0; i < g.taille; i+=1) {
        for(int j=0; j < g.taille; j+=1) {
            if (g.cases[i][j] == CROIX) {
                n += 1;
            }
        }
    }
    return n;
}

int nombre_de_solutions(grille g, int ligne, int colonne) {
    // Si on a rempli toutes les cases de la grille, c'est une solution valide
    if (ligne == g.taille) {
        if (nombre_croix(g) == g.taille) {
            return 1;
        }
        else {
            return 0;
        }
    }
    
    int nb_sol = 0;
    
    // Passer à la case suivante
    int prochaine_ligne = ligne;
    int prochaine_colonne = colonne + 1;
    if (prochaine_colonne == g.taille) {
        prochaine_colonne = 0;
        prochaine_ligne += 1;
    }
    
    // Essayer toutes les possibilités pour cette case
    if (case_valide(g, ligne, colonne)) {
        g.cases[ligne][colonne] = CROIX;
        nb_sol += nombre_de_solutions(g, prochaine_ligne, prochaine_colonne);
        g.cases[ligne][colonne] = VIDE;
    }
    nb_sol += nombre_de_solutions(g, prochaine_ligne, prochaine_colonne);
    
    return nb_sol;
}

// Fonction qui renvoie le nombre de solutions pour une grille de taille n x n
int vfinale(int n) {
    // Initialiser une grille vide de taille n
    grille g = initialiser_grille(n);

    // Compter le nombre de solutions possibles en remplissant la grille case par case
    int nb_solutions = nombre_de_solutions(g, 0, 0);

    // Libérer la mémoire allouée pour la grille
    detruire_grille(&g);

    // Renvoyer le nombre de solutions trouvées
    return nb_solutions;
}

// Fonction vfinale lorsque le main est celui avec en paramètre un fichier que l'on doit ouvrir
/*
void vfinale(char* nom_fichier) {
    // Lire l'entier n depuis le fichier
    int n = 0;
    FILE* fichier = fopen(nom_fichier, "r");
    if (fichier != NULL) {
        fscanf(fichier, "%d", &n);
        fclose(fichier);
    } else {
        printf("Erreur : Impossible de lire le fichier %s\n", nom_fichier);
        return;
    }
    
    // Vérifier si n est valide
    if (n < 1) {
        printf("Erreur : n doit être >= 1\n");
    }
    else {
    // Calculer le nombre de solutions pour une grille de taille n x n
    grille g = initialiser_grille(n);
    int nb_solutions = nombre_de_solutions(g, 0, 0); // À compléter

    // Libérer la mémoire allouée pour la grille
    detruire_grille(&g);
    
    // Afficher le résultat
    printf("%d\n", nb_solutions);
    }
}
*/

int main() {
    int n = vfinale(5);
    printf("%d", n);
    return 0;
}

// Main lorsque l'on prend en paramètre un fichier
/*
int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s fichier\n", argv[0]);
        return EXIT_FAILURE;
    }
    FILE *fichier = fopen(argv[1], "r");
    if (fichier == NULL) {
        printf("Erreur: impossible d'ouvrir le fichier %s\n", argv[1]);
        return EXIT_FAILURE;
    }
    int n;
    if (fscanf(fichier, "%d", &n) != 1) {
        printf("Erreur: le fichier %s ne contient pas un entier\n", argv[1]);
        return EXIT_FAILURE;
    }
    fclose(fichier);

    if (n == 1) {
        printf("Le problème n'a pas de solution pour n=1\n");
        return EXIT_SUCCESS;
    } else if (n == 2) {
        printf("Le problème n'a pas de solution pour n=2\n");
        return EXIT_SUCCESS;
    }
    grille g = initialiser_grille(n);
    int nb_solutions = nombre_de_solutions(g, 0, 0);
    printf("Le nombre de solutions pour une grille de taille %d est %d\n", n, nb_solutions);

    detruire_grille(&g);
    return EXIT_SUCCESS;
}
*/
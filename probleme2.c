#include "probleme2.h"

bool conservable (int immeuble_devant, int immeuble_derriere){
    return (immeuble_devant < immeuble_derriere);
}

int max (int a, int b){
    if (a >= b){
        return a;
    }
    else{
        return b;
    }
}
int version1 (int immeuble_front, int nb_immeubles, int immeubles[]){
    //Cette fonction n'est qu'une fonction auxiliaire de v1
    if (nb_immeubles == 1 && conservable (immeuble_front, immeubles[0])){
        return 1;//Si on a un seul immeuble à tester, et qu'on peut le garder, il est toujours plus intéressant de le garder.
    }
    else {
        if (nb_immeubles == 1){
            return 0;
        }
        else{
            if (conservable (immeuble_front, immeubles[0])){
                int* immeubles_ = malloc ((nb_immeubles - 1) * sizeof(int));
                for (int i = 0; i < nb_immeubles - 1; i += 1){
                    immeubles_[i] = immeubles[i+1];
                };
                int conserve = 1 + version1(immeubles[0], nb_immeubles - 1, immeubles_);//max d'immeuble pouvant etre conservé si on ne détruit pas l'immeuble
                int non_conserve = version1(immeuble_front, nb_immeubles - 1, immeubles_);//max d'immeuble pouvant etre conservé si on détruit l'immeuble.
                free (immeubles_);
                return (max(conserve, non_conserve));
            }
            else{
                int* immeubles__ = malloc ((nb_immeubles - 1) * sizeof(int));
                for (int i = 0; i < nb_immeubles - 1; i += 1){
                    immeubles__[i] = immeubles[i + 1];
                };//ici, on ne peut pass conserver l'immeuble car celui de devant est trop grand
                int rep = version1(immeuble_front, nb_immeubles - 1, immeubles__);
                free (immeubles__);
                return (rep);
            }
        }
    }
}


int v1(int nb_immeubles,int immeubles[]){
    if (nb_immeubles == 1){return 1;};
    int* immeubles_ = malloc (nb_immeubles * sizeof(int));
    for (int i = 0; i < nb_immeubles - 1; i += 1){
        immeubles_[i] = immeubles[i+1];
    };
    int conserve = 1 + version1 (immeubles[0], nb_immeubles - 1, immeubles_);
    int non_conserve = v1 (nb_immeubles - 1, immeubles_);
    free (immeubles_);
    return (max (conserve, non_conserve));
}//l'idee est la même que pour version 1 sauf qu'on a pas de premier immeuble de front donc il faut le choisir nous même pour se ramener ensuite à version1.(on essaye toutes les possibilités).

int max_tableau(int taille, int* tab){
    int max = 0;//Dans notre cas, ce n'est pas un problème de mettre le max à 0 par défaut car on ne manipule que des valeurs positives
    for (int i = 0; i < taille; i += 1){
        if (tab[i] > max){
            max = tab[i];
        }
    };
    return (max);
}

void remplir_v2 (int nb_immeubles, int immeubles[], int solutions[], int n){//Cette fonction sert à remplir le tableau des solutions de v2
    if (solutions[n] == 0){
        remplir_v2 (nb_immeubles, immeubles, solutions, n+1);
        int* tab = malloc((nb_immeubles - n) * sizeof(int));//Ce tableau contiendra les solutions optimales avec n comme premier immeuble
        for (int i = 0; i < nb_immeubles - n ;i +=1){
            if (immeubles[n] < immeubles[i + n]){
                tab[i] = 1 + solutions[i + n];// Si on trouve un immeuble plus grand que le n-ième dans la suite du tableau, alors la solution en partant de cette immeuble à laquelle on ajoute le n-ième est valide.
            }
            else{
                tab[i] = -1;
            };
        };
        solutions[n] = max(1, max_tableau(nb_immeubles - n, tab));
        free (tab);
    };
}

int v2(int nb_immeubles, int immeubles[]){
    int* solutions = malloc (nb_immeubles * sizeof(int));
    for (int i = 0; i < nb_immeubles; i += 1){
        solutions[i] = 0;
    };
    solutions[nb_immeubles - 1] = 1;
    remplir_v2 (nb_immeubles, immeubles, solutions, 0);
    int rep = max_tableau(nb_immeubles, solutions);
    free (solutions);
    return (rep);
}

int v3(int nb_immeubles, int immeubles[]){
    int* solutions = malloc(nb_immeubles * sizeof(int));
    for (int i = nb_immeubles - 1; i >= 0 ; i -= 1){
        solutions[i] = 1;
        for (int j = i + 1 ; j < nb_immeubles; j += 1){//L'idée est la même que pour la fonction précédente
            if (immeubles[i] < immeubles[j]){
                solutions[i] = max(solutions[i],1 + solutions[j]);
            };
        };
    };
    int rep = max_tableau (nb_immeubles, solutions);
    free(solutions);
    return (rep);
}

int vfinale(int nb_immeubles, int immeubles[]){
    return (v3(nb_immeubles, immeubles));
}

int main(int argc, char** argv){
    assert(argc == 2);
    FILE* doc = fopen (argv[1], "r");
    int taille;
    fscanf(doc, "%d\n", &taille);
    int* t = malloc(taille * sizeof(int));
    for(int i = 0; i < taille; i += 1){
        fscanf(doc, "%d\n", &t[i]);
    };
    printf("%d\n", vfinale(taille, t));
    free(t);
    fclose (doc);
}
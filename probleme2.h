#ifndef _probleme2_h_
#define _pobleme2_h_
#include "librairies.h"
/*Membres du groupe ayant participé à cette version:
        *Mathieu
        *Thibault
        *Kellian
Cette fonction impléménte une résolution naïve du problème 2 :
    Pour chaque immeuble de la liste, on regarde si on peut le garder ou non, et on regarde les différentes possiblités en le gardant,
     mais aussi en le détruisant, pour à la fin garder le nombre maximal d'immeubles.
La compléxité de cette fonction est O(n*exp(n))
Preuve de correction/terminaison : par récurrence avec Hn : "cette fonction termine et donne la solution optimale pour un tableau de taille n." 
*/
int v1(int nb_immeubles, int immeubles[]);

/*Membres du groupe ayant participé à cette version :
        *Mathieu
        *Kellian
Cette fonction implémente une résolution par programmation dynamique (mémoïsation) :
        Pour chaque immeuble de la liste, on cherche à résoudre le sous-problème pour lequel cet immeuble est le premier.
        On sait déjà que pour le dernier, la solution est 1.
La compléxité de cette fonction est O(n²)
Preuve de correction/terminaison : par récurrence avec Hn : "Cette fonction termine et donne la solution optimale pour un tableau de taille n."
*/
int v2(int nb_immeubles, int immeubels[]);

/* Membres du groupe ayant participé à cette version :
        *Mathieu
        *Thibault
Cette fonction implémente une résolution par programmation dynamique (bottom-up):
        Pour chaque immeuble de la liste, on cherche à résoudre le sous-problème pour lequel cet immeuble est le premier.
        On sait déjà que pour le dernier, la solution est 1.
La compléxité de cette fonxtion est O(n²)(On a deux boucles imbriquées)
Inv(j) = solutions[i] vaut la manière optimale de garder les immeubles avec comme premier immeubles l'indice i et en second l'un des immeubles entre i et j si il existe j tel que immueuble[i] < immeubles[j]
Inv(i) = solutions [i] vaut la manière optimale de garder les immeubles avec comme premier immeuble celui d'indice i.
Var(i) = i
*/
int v3(int nb_immeubles, int immeubles[]);
#endif
